﻿namespace AuctionProto
{
    /// <summary>
    /// Class paired to appsettings.json
    /// </summary>
    public class AppSettings
    {
        public uint DefaultTimeoutMin { get; set; }
        public uint DefaultTimeoutMax { get; set; }
        public uint DefaultAuctionStartAmount { get; set; }
        public uint MinDelayBeforeNewAuction { get; set; }
        public uint PostBidBuffer { get; set; }
    }
}