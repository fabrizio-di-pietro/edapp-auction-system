﻿# Lightning-Auction World

## Execution
1) Clone repo
2) Open solution
3) Change appsettings.jeon to configure auctions behaviour
4) Execute via IIS Express
## Dependencies
The system is based on dotnet core 3.0. [Download and install](https://dotnet.microsoft.com/download/dotnet-core/3.0) if not installed already on your machine.
## How to use
User changes by "simply" changing the name in the input box The username string is the representation of the full user. A full environment would take care of a logged-in user passing its object instead of a string representing the user.

New auctions and items are randomly spawned every "MinDelayBeforeNewAuction" seconds. Its value can be modified in appsettings.json.

Referring to the "Auctions" column:
 - White auctions are incoming ones. They are disabled via bootstrap class ".disable"
 - Blue auctions are active ones. They are clickable ones.

To verify webApp's behaviour, open two tabs or two different browsers.

Data is not retained in any database.
## Improvements
 - Write unit tests (* requirement)
 - Get "Current state" when opening the page for the first time
 - Live update of Item cards
 - Create live countdown for expiring auctions
 - Live update of most-recent price in "Auctions" list
 - Improve back-end data check
 - Graphics
 - Create recap pages
 - Handle exceptions
 - Use NLog as logging system
 
## Limitations
- When two pages are opened, the second one behaves as a new one without showing the state of existing offers, so it has to "build up" before reaching almost the same state as the first one
- The current system wouldn't scale


### Tech

Based on dotnet core 3.0 WebApp MVC boilerplate.
Main tech:
* [DotNet Core 3.0](https://dotnet.microsoft.com/download/dotnet-core/3.0)
* [SignalR](https://docs.microsoft.com/en-us/aspnet/signalr/overview/getting-started/introduction-to-signalr) - built-in in DotNet Core 3.0
* [jQuery](https://jquery.com)
* [Bootstrap](https://getbootstrap.com)
