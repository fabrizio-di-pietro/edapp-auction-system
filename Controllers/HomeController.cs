﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AuctionProto.Models;
using AuctionProto.Services;

namespace AuctionProto.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IAuctionService _auctionService;

        public HomeController(ILogger<HomeController> logger, IAuctionService auctionService)
        {
            _logger = logger;
            _auctionService = auctionService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet, Route("auction/{refKey}")]
        public async Task<IActionResult> GetAuction(int refKey)
        {
            return PartialView("_AuctionPartial", await _auctionService.GetAuction(refKey));
        }

        [HttpPost, Route("auction/{refKey}")]
        public async Task<bool> PostAuction(int refKey, [FromForm] string user, [FromForm] decimal bid)
        {
            return await _auctionService.BidAuction(user, refKey, bid);
        }
    }
}
