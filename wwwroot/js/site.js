﻿var messagesList = document.getElementById("activity-report");
var auctions = document.getElementById("auctions-list");

var connection = new signalR.HubConnectionBuilder()
    .withUrl("/auctionHub")
    .build();

connection.on("Notify", function (message) {
    appendMessage(message);
});

connection.on("AuctionEnded", function (refKey) {
    updateAuction(refKey, "end");
});

connection.on("AddAuctions", function (auction) {
    addAuction(auction);
    addAuctionListener(auction);
});

connection.on("AuctionStarted", function (refKey) {
    updateAuction(refKey, "start");
});

connection.start().catch(err => console.error(err));

var connection2 = new signalR.HubConnectionBuilder()
    .withUrl("/auctionHub")
    .build();

connection2.on("ReceiveMessage", (user, message) => {
    const encodedMsg = user + " says " + message;
    const li = document.createElement("li");
    li.innerHTML = encodedMsg;
    document.getElementById("messagesList").appendChild(li);
});

connection2.on("Bids", function (message) {
    const li = document.createElement("li");
    li.innerHTML = "<strong>" + message + "</strong>";
    document.getElementById("messagesList").appendChild(li);
});

connection2.start().catch(err => console.error(err));


function appendMessage(content) {
    var li = document.createElement("li");
    li.innerText = content;
    messagesList.appendChild(li);
    if (messagesList.childNodes.length > 20) {
        messagesList.firstChild.remove();
    }
}

function addAuction(auction) {
    var li = document.createElement("li");
    li.id = "id-" + auction.idAuction;
    li.classList.add("list-group-item", "disabled");
    li.innerHTML = auction.item.name + " - " + auction.startingPrice;
    auctions.appendChild(li);
}

function updateAuction(refKey, action) {
    var elm = document.getElementById("id-" + refKey);
    if (action === "end") {
        elm.remove();
    }
    else {
        elm.classList.remove("disabled");
        elm.classList.add("active");
    }
}
function addAuctionListener(auction) {
    document.getElementById("id-" + auction.idAuction).addEventListener("click", event => {
        event.preventDefault();
        var auctionPanel = document.getElementById("auction-bid");
        auctionPanel.innerHTML = "";
        $.get("auction/" + auction.idAuction)
            .done(function (d) {
                auctionPanel.innerHTML = d;
                document.getElementsByName("place-bid")[0].addEventListener("click", e => {
                    var user = document.getElementById("user-input").value;
                    $.post("auction/" + auction.idAuction, {
                        user: user,
                        bid: document.getElementsByName("bid-value")[0].value
                    })
                        .done(function (d) {
                            if (!d) {
                                alert("Bid was invalid!");
                            }
                        });
                    e.preventDefault();
                });
            });
    });
}