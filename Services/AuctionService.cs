﻿using AuctionProto.Hubs;
using AuctionProto.Models;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace AuctionProto.Services
{
    public interface IAuctionService
    {
        Task<Auction> GetAuction(int idAuction);
        Task<bool> BidAuction(string user, int idAuction, decimal price);
    }
    public class AuctionService : IAuctionService
    {
        private readonly AppSettings _appSettings;
        private readonly Random _random = new Random();
        private readonly IHubContext<AuctionHub> _auctionHubContext;

        private Timer _timer;

        private static ConcurrentDictionary<int, Auction> _activeAuctions = new ConcurrentDictionary<int, Auction>();
        private static ConcurrentDictionary<int, Auction> _endedAuctions = new ConcurrentDictionary<int, Auction>();

        public AuctionService(IOptions<AppSettings> appSettings, IHubContext<AuctionHub> auctionHubContext, IHostApplicationLifetime hostApplicationLifetime)
        {
            _appSettings = appSettings.Value;
            _auctionHubContext = auctionHubContext;
            hostApplicationLifetime.ApplicationStarted.Register(() => Demon());
        }

        public async Task<Auction> GetAuction(int idAuction)
        {
            if (_activeAuctions.TryGetValue(idAuction, out Auction auction))
            {
                return auction;
            }
            return null;
        }

        public async Task<bool> BidAuction(string user, int idAuction, decimal price)
        {
            var res = false;
            var isAuctionAvailable = _activeAuctions.TryGetValue(idAuction, out Auction auction);

            if (isAuctionAvailable && auction.Biddable && price >= auction.StartingPrice)
            {
                bool hasBid = auction.CurrentBid != null;
                decimal refPrice = hasBid ? auction.CurrentBid.BidPrice : auction.StartingPrice;

                if ((hasBid && price > refPrice) || (!hasBid && price >= refPrice))
                {
                    auction.CurrentBid = new Bid()
                    {
                        IdItem = auction.Item.IdItem,
                        User = user,
                        BidPrice = price
                    };
                    if ((DateTime.UtcNow - auction.EndTime).TotalSeconds < _appSettings.PostBidBuffer)
                    {
                        auction.AddSecondsToEndTime(_appSettings.PostBidBuffer);
                    }
                    SendBidMessageAsync(auction, user + " just bid " + price + " for " + auction.Item.Name);
                    res = true;
                }
            }
            return res;
        }

        /// <summary>
        /// Demon to summon new auctions every second
        /// </summary>
        private void Demon()
        {
            _timer = new Timer(x => ((AuctionService)x).AuctionsDemon(), this, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        private void AuctionsDemon()
        {
            foreach (var auction in _activeAuctions)
            {
                if (auction.Value.EndTime < DateTime.UtcNow && _activeAuctions.TryRemove(auction.Key, out Auction foundAuction))
                {
                    auction.Value.Biddable = false;
                    foundAuction.Biddable = false;
                    _endedAuctions.GetOrAdd(foundAuction.IdAuction, foundAuction);

                    // send end signal -- fire and forget
                    SendAuctionEndedAsync(foundAuction);
                }
                else if (!auction.Value.Biddable && auction.Value.StartTime <= DateTime.UtcNow)
                {
                    auction.Value.Biddable = true;

                    // send start signal -- fire and forget
                    SendAuctionStartedAsync(auction.Key);
                }
            }
            // add another auction to the list no more often than every MinDelayBeforeNewAuction seconds
            if (DateTime.UtcNow.Second % _appSettings.MinDelayBeforeNewAuction == 0)
            {
                // NOTE: hard-coded implementation to mimic new auctions available.
                Auction newAuction = PopulateAuctions();

                // send added signal -- fire and forget
                SendAuctionAddedAsync(newAuction);
            }

        }

        private async Task SendAuctionEndedAsync(Auction auction)
        {
            _auctionHubContext.Clients.All.SendAsync("Notify", DateTime.UtcNow.ToShortTimeString() + " End auction (" + auction.IdAuction + ")");
            _auctionHubContext.Clients.All.SendAsync("AuctionEnded", auction.IdAuction);
            if (auction.CurrentBid != null)
            {
                _auctionHubContext.Clients.All.SendAsync("Bids", DateTime.UtcNow.ToShortTimeString() + ": " + auction.Item.Name + " paid " + auction.CurrentBid.BidPrice + " and <span class=\"bg-danger\">won by " + auction.CurrentBid.User + "</span>");
            }
        }

        private async Task SendAuctionAddedAsync(Auction auction)
        {
            _auctionHubContext.Clients.All.SendAsync("Notify", DateTime.UtcNow.ToShortTimeString() + " New auction (" + auction.IdAuction + ")");
            _auctionHubContext.Clients.All.SendAsync("AddAuctions", auction);
        }

        private async Task SendAuctionStartedAsync(int refKey)
        {
            _auctionHubContext.Clients.All.SendAsync("Notify", DateTime.UtcNow.ToShortTimeString() + " Open auction (" + refKey + ")");
            _auctionHubContext.Clients.All.SendAsync("AuctionStarted", refKey);
        }

        private async Task SendBidMessageAsync(Auction auction, string message)
        {
            _auctionHubContext.Clients.All.SendAsync("Bids", DateTime.UtcNow.ToShortTimeString() + ": " + auction.Item.Name + " " + message);
        }

        /// <summary>
        /// Auctions Generator
        /// </summary>
        /// <returns></returns>
        private Auction PopulateAuctions()
        {
            Auction auction = GenerateFakeAuction();
            _activeAuctions.GetOrAdd(auction.IdAuction, auction);
            return auction;
        }


        private Auction GenerateFakeAuction()
        {
            Item techItem = new Item { IdItem = _random.Next(0, 99) };
            techItem.Name = "Tech Item#" + techItem.IdItem;

            Item personalCareItem = new Item { IdItem = _random.Next(100, 199) };
            personalCareItem.Name = "Personal Care Item#" + personalCareItem.IdItem;

            Item houseItem = new Item { IdItem = _random.Next(200, 299) };
            houseItem.Name = "House Item#" + houseItem.IdItem;

            Item bookItem = new Item { IdItem = _random.Next(300, 399) };
            bookItem.Name = "Book Item#" + bookItem.IdItem;

            Item carItem = new Item { IdItem = _random.Next(400, 499) };
            carItem.Name = "Car Item#" + carItem.IdItem;

            Item musicItem = new Item { IdItem = _random.Next(500, 599) };
            musicItem.Name = "Music Item#" + musicItem.IdItem;

            Item gameItem = new Item { IdItem = _random.Next(600, 699) };
            gameItem.Name = "Game Item#" + gameItem.IdItem;


            var items = new[] { techItem, personalCareItem, houseItem, bookItem, carItem, musicItem, gameItem };

            var startTime = DateTimeOffset.UtcNow.AddSeconds(_random.Next(Math.Min(5, (int)_appSettings.DefaultTimeoutMin), (int)_appSettings.DefaultTimeoutMin));
            var endTime = startTime.AddSeconds(_random.Next((int)_appSettings.DefaultTimeoutMin, (int)_appSettings.DefaultTimeoutMax));
            var startingPrice = _random.Next(1, 100);
            return new Auction(items[_random.Next(0, items.Length - 1)], startTime, endTime, startingPrice);
        }
    }
}