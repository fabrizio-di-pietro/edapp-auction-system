﻿namespace AuctionProto.Models
{
    public class Item
    {
        public int IdItem { get; set; }
        public string Name { get; set; }
    }
}
