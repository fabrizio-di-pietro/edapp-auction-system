﻿namespace AuctionProto.Models
{
    public class Bid
    {
        public string User { get; set; }
        public int IdItem { get; set; }
        public decimal BidPrice { get; set; }
    }
}
