﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace AuctionProto.Models
{
    public class Auction
    {
        static int _idAuction = 0;

        public Auction(Item item, DateTimeOffset startTime, DateTimeOffset endTime, decimal startingPrice)
        {
            IdAuction = Interlocked.Increment(ref _idAuction);
            Item = item;
            StartTime = startTime;
            EndTime = endTime;
            StartingPrice = startingPrice;
            Biddable = false;
        }

        public int IdAuction { get; }
        public Item Item { get; }
        public DateTimeOffset StartTime { get; }
        public DateTimeOffset EndTime { get; }
        public decimal StartingPrice { get; }
        public bool Biddable { get; set; }
        public Bid CurrentBid { get; set; }
        public List<Bid> BidsAllTime { get; set; }

        public void AddSecondsToEndTime(uint secondsToAdd)
        {
            EndTime.AddSeconds(secondsToAdd);
        }
    }
}
